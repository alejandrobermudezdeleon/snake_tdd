# -Wall habilita todas las advertencias y -g incluye información de depuración 
flags := -Wall -g -Isrc -Itest

# La opción -fPIC en GCC indica que el código debe ser compilado como "Position 
# Independent Code", lo que permite su uso en bibliotecas compartidas y garantiza 
# la portabilidad y flexibilidad en tiempo de ejecución, porque el código es más 
# portable y evita problemas de fragmentación de memoria al cargar y descargar 
# bibliotecas compartidas en tiempo de ejecución. Es importante tener en cuenta 
# que la opción -fPIC se utiliza al compilar el código fuente, no al enlazarlo. 
# Durante el proceso de enlace, es posible que también se requieran opciones 
# adicionales, como -shared, para generar una biblioteca compartida final.
fPIC := -fPIC -I.jni

# La función wildcard obtiene la lista de todos los archivos .c en la carpeta src 
# y de la capeta test, y los almacena en las variable src_files y test_files. 
src_files  := $(wildcard src/*.c)
test_files := $(wildcard test/*.c)

# Se utiliza $(notdir $(basename $(src_files))) y $(notdir $(basename $(test_files))) 
# para obtener solo los nombres base de los archivos sin la extensión .c, que se 
# asignan a las variables libraries y tests. También funciona:
#       "libraries := $(patsubst src/%.c,%,$(src_files))". 
#       "tests     := $(patsubst test/%.c,%,$(test_files))". 
libraries := $(notdir $(basename $(src_files)))
tests     := $(notdir $(basename $(test_files)))

greenfoot_JAR := .jar/greenfoot.jar:.jar/turnbased.jar
JUnit_JAR     := .jar/org.junit_4.13.2.v20211018-1956.jar

# Las funciones addprefix y addsuffix generan el nombre de todas las reglas asociadas
# a la generación de las bibliotecas _jni.so y Test_jni.so, utilizando la lista de 
# nombres que hay en la variable libraries:
#
#      run/GreenfootFrame: $(addprefix .jni/,$(addsuffix _jni.so,$(libraries))) \
#                        $(addprefix .jni/,$(addsuffix Test_jni.so,$(libraries)))
#
# Otra forma de conseguir lo mismo es poner una regla que tenga como dependencia
# las reglas que generan las bibliotecas _jni.so y Test_jni.so:
#
#      run/GreenfootFrame: $(addprefix run/,$(libraries))
#
#      define library_rule
#      run/$(1): .jni/$(1)_jni.so .jni/$(1)Test_jni.so
#      ...
#
# Otra solución es utilizar iteradores para generar solo el nombre de las reglas 
# asociadas a bibiotecas _jni.c o Test_jni.c que existan en .jni:
#
#      generated_rules := $(foreach lib,$(libraries),$(if $(wildcard .jni/$(lib)_jni.c),.jni/$(lib)_jni.so)) \
#                         $(foreach lib,$(libraries),$(if $(wildcard .jni/$(lib)Test_jni.c),.jni/$(lib)Test_jni.so))
#
# La solución final que se ha elegido es procesar de forma separada las bibliotecas 
# y los tests. Primero se filtra la variable libraries, quedandonos en la variable 
# jni_libraries solo los nombres que cumplen tener en .jni el fichero asociado _jni.c
# y luego se utiliza jni_libraries para almacenar en libraries_rules el nombre de las
# reglas asociadas a los ficheros _jni.so que se deben generar. El proceso se repite
# con los tests, es decir, se filtra la variable tests para calcular la variable 
# jni_tests y luego se utiliza esta variable para almacenar en tests_rules el nombre  
# de las reglas asociadas a los ficheros Test_jni.so que se deben generar. 
jni_libraries   := $(filter-out $(foreach lib,$(libraries),$(if $(wildcard .jni/$(lib)_jni.c),,$(lib))),$(libraries)) 
libraries_rules := $(foreach lib,$(jni_libraries),.jni/$(lib)_jni.so) 

jni_tests   := $(filter-out $(foreach lib,$(tests),$(if $(wildcard .jni/$(lib).jni),,$(lib))),$(tests)) 
tests_rules := $(foreach lib,$(jni_tests),.jni/$(lib)_jni.so) 

run/GreenfootFrame: $(libraries_rules) $(tests_rules) .jni/Scenarios.class
	@gcc -c $(flags) .jni/GreenfootFrame.c -o .jni/GreenfootFrame.o
	gcc $(flags) .jni/GreenfootFrame.o -o run/GreenfootFrame
	
.jni/Scenarios.class: .jni/Scenarios.java
	@echo "public class Scenarios {" > .jni/Scenarios.java
	@cat .jni/*.scenarios | sort | uniq >> .jni/Scenarios.java
	@echo "" >> .jni/Scenarios.java
	@echo "}" >> .jni/Scenarios.java
	@javac -cp ".jni:$(greenfoot_JAR):$(JUnit_JAR)" $<

.jni/Scenarios.java:
	@echo "public class Scenarios {}" > .jni/Scenarios.java

# La función foreach de GNU Make itera sobre los nombres de los archivos almacenados 
# en la variable jni_libraries, generando cuatro reglas por cada nombre a partir de
# las reglas definidas en library_rule_maker. Este proceso también se realiza para
# generar las reglas asociadas a los tests. La función foreach debe estar después 
# de la definición de define ... endef.
#
# Dentro del define se puede poner: 
#                                    "gcc -c  $(flags)  $(fPIC) $$< -o $$@" 
#                                  o
#                                    "gcc -c $$(flags) $$(fPIC) $$< -o $$@" 
#
# La diferencia se encuentra en la forma en que se interpretan las variables en el 
# contexto de la regla de construcción. Cuando se pone $$, la variable está siendo 
# escapada y lo que indica es que no se expanda durante la fase de expansión del 
# makefile, pasándose literalmente al shell para su ejecución. El shell será responsable 
# de expandir y evaluar las variables $(flags) y $(fPIC) en el momento de ejecución.
define library_rules_maker
.jni/$(1)_jni.so: .jni/$(1)_jni.o .jni/$(1).class
	@gcc -shared $(flags) $(fPIC) $$< -o $$@

.jni/$(1)_jni.o: .jni/$(1)_jni.c .jni/$(1).o
	@gcc -c $(flags) $(fPIC) $$< -o $$@

.jni/$(1).o: src/$(1).c
	gcc -c $(flags) $(fPIC) $$< -o $$@

.jni/$(1).class: .jni/$(1).java
	@javac -cp ".jni:$(greenfoot_JAR)" $$<
endef
$(foreach lib,$(jni_libraries),$(eval $(call library_rules_maker,$(lib))))

define test_rules_maker
.jni/$(1)_jni.so: .jni/$(1)_jni.o .jni/$(1).class
	@gcc -shared $(flags) $(fPIC) $$< -o $$@

.jni/$(1)_jni.o: .jni/$(1)_jni.c .jni/$(1).o
	@gcc -c $(flags) $(fPIC) $$< -o $$@

# La regla ".jni/$(1).o" que compila un test debe depender de los fuentes que 
# están en test: "test/$(1).c", y en src: src/$(1){sin la terminación Test}.c 
# Para eliminar Test de $1 (es decir, quitar los últimos cuatro caracteres) 
# se hace: "$(shell echo $(patsubst %,%,$1) | rev | cut -c 5- | rev)"
.jni/$(1).o: test/$(1).c src/$(shell echo $(patsubst %,%,$1) | rev | cut -c 5- | rev).c
	gcc -c $(flags) $(fPIC) $$< -o $$@

.jni/$(1)_jni.c: .jni/$(1).o
	@echo "/**" > $$@
	@echo " * @author Francisco Guerra (francisco.guerra@ulpgc.es)" >> $$@
	@echo " * @version 1.0" >> $$@
	@echo " */" >> $$@
	@echo "" >> $$@
	@echo \#include \"$1.c\" >> $$@
	@echo "extern JNIEnv *javaEnv;" >> $$@
	@echo "" >> $$@
	@nm $$< | grep ' T ' | cut -d ' ' -f 3 | grep 'scene' | sed 's/^_//' | sed 's/_/_1/g' | awk -v var=$(1) 'BEGIN {ORS="";}{print "JNIEXPORT void JNICALL Java_";print var;print "_";print $$0;print "\n  (JNIEnv *env, jobject object, jobject board)\n{\n";print "    javaEnv = env;\n    ";print $$0;print "(board);\n";print "}\n\n"}' | sed '/^    scene/s/_1/_/g' >> $$@	
	@nm $$< | grep ' T ' | cut -d ' ' -f 3 | grep 'test'  | sed 's/^_//' | sed 's/_/_1/g' | awk -v var=$(1) 'BEGIN {ORS="";}{print "JNIEXPORT void JNICALL Java_";print var;print "_";print $$0;print "\n  (JNIEnv *env, jobject object)\n{\n";print "    javaEnv = env;\n    ";print $$0;print "();\n";print "}\n\n"}' | sed '/^    test/s/_1/_/g' >> $$@	

.jni/$(1).java: .jni/$(1).o
	@echo "/**" > $$@
	@echo " * @author Francisco Guerra (francisco.guerra@ulpgc.es)" >> $$@
	@echo " * @version 1.0" >> $$@
	@echo " */" >> $$@
	@echo "" >> $$@
	@echo "import org.junit.Test;" >> $$@
	@echo "import org.junit.runner.RunWith;" >> $$@
	@echo "import greenfoot.junitUtils.runner.GreenfootRunner;" >> $$@
	@echo "@RunWith(GreenfootRunner.class)" >> $$@
	@echo "public class $1 {" >> $$@
	@nm $$< | grep ' T ' | cut -d ' ' -f 3 | grep 'scene' | sed 's/^_//' | awk 'BEGIN {ORS="";}{print "    public native void ";print $$0;print "(greenfoot.World board);\n\n"}' >> $$@
	@nm $$< | grep ' T ' | cut -d ' ' -f 3 | grep 'test'  | sed 's/^_//' | awk 'BEGIN {ORS="";}{print "    @Test\n";print "    public native void ";print $$0;print "();\n\n"}' >> $$@
	@echo "    static {" >> $$@
	@echo "        System.load(new java.io.File(\".jni\", \"$1_jni.so\").getAbsolutePath());" >> $$@
	@echo "    }" >> $$@
	@echo "}" >> $$@
	@rm -f .jni/Scenarios.class
	@rm -f .jni/$1.scenarios
	@nm $$< | grep ' T ' | cut -d ' ' -f 3 | grep 'scene' | sed 's/^_//' | awk 'BEGIN {ORS="";}{print "\n    public void ";print "$1_";print $$0;print "(greenfoot.World board) {";print" new ";print "$(1)";print"().";print $$0;print"(board);";print" }"}' >> .jni/$1.scenarios

.jni/$(1).class: .jni/$(1).java
	@javac -cp ".jni:$(greenfoot_JAR):$(JUnit_JAR)" $$<
endef
$(foreach lib,$(jni_tests),$(eval $(call test_rules_maker,$(lib))))
